create database database_lookingJobs;
use database_lookingJobs;

CREATE TABLE IF NOT EXISTS telefono(
	id_telefono INT AUTO_INCREMENT PRIMARY KEY,
	movil VARCHAR(10),
    convencional VARCHAR(10)
);
CREATE TABLE IF NOT EXISTS direccion(
	id_direccion INT AUTO_INCREMENT PRIMARY KEY,
    callePrincipal VARCHAR(50),
	calleSecundaria VARCHAR(50),
    ciudad VARCHAR(50)
);
CREATE TABLE IF NOT EXISTS usuario(
	id_usuario INT  AUTO_INCREMENT PRIMARY KEY,
	nombre VARCHAR(50) NOT NULL, 
	apellido VARCHAR(50) NOT NULL,
    email VARCHAR(30) NOT NULL,
    contrasena VARCHAR(50) NOT NULL,
	id_telefono INT,
    id_direccion INT,
    FOREIGN KEY (id_direccion) REFERENCES direccion(id_direccion),
	FOREIGN KEY (id_telefono) REFERENCES telefono(id_telefono)
);

CREATE TABLE IF NOT EXISTS cliente(
	id_cliente INT AUTO_INCREMENT PRIMARY KEY,
	descripcion VARCHAR(300),
    id_usuario INT,
    FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario)
);
CREATE TABLE IF NOT EXISTS servicio(
	id_servicio INT AUTO_INCREMENT PRIMARY KEY,
	descripcion VARCHAR(300)
);
CREATE TABLE IF NOT EXISTS trabajador(
	id_trabajador INT AUTO_INCREMENT PRIMARY KEY,
	descripcion VARCHAR(300),
    id_usuario INT,
    id_profesion INT,
    calificacion INT,
    FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario),
	FOREIGN KEY (id_profesion) REFERENCES servicio(id_servicio)
);

CREATE TABLE IF NOT EXISTS contrato(
	id_contrato INT AUTO_INCREMENT PRIMARY KEY,
	descripcion VARCHAR(300),
    id_cliente INT,
    id_trabajador INT,
    fecha DATETIME,
    FOREIGN KEY (id_cliente) REFERENCES cliente(id_cliente),
	FOREIGN KEY (id_trabajador) REFERENCES trabajador(id_trabajador)
);