import React from 'react';
import Header from './components/Header'
import {BrowserRouter as Router, Route} from 'react-router-dom'
import Footer from './components/Footer'
import ComoFunciona from './components/ComoFunciona'
import Inicio from './components/pages/Inicio'
import Acerca from './components/pages/Acerca'
import Trabajos from './components/pages/Trabajos'
import Contactenos from './components/pages/Contactenos'
import Noticias from './components/pages/Noticias'
import Login from './components/pages/Login'
import Registro from './components/pages/Registro'
import Team_work from './components/pages/Team_work'
import Profile from './components/pages/Profile'
function App() {
  return (
    <div>
      <Router>
      <Header/>
      <Route path="/" exact component={Inicio}/>
      <Route path="/trabajos"  component={Trabajos}/>
      <Route path="/acerca"  component={Acerca}/>
      <Route path="/noticias"  component={Noticias}/>
      <Route path="/contactenos"  component={Contactenos}/>
      <Route path="/registro"  component={Registro}/>
      <Route path="/login"  component={Login}/>
      <Route path="/team_work"  component={Team_work}/>
      <Route path="/profile"  component={Profile}/>

     </Router>
    <ComoFunciona/>
      <Footer/>
      
    </div>
   
  );
}
export default App;