import React from 'react';
import {Link} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/Header.css'
import logo from '../img/logo/logo.png';
class Header extends React.Component{

    render(){
        return (
            <div>
            <div className="header-area header-transparrent">
            <div className="headder-top header-sticky">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-lg-3 col-md-2">                          
                      <a href="/"><img src={logo} /></a>  
                        </div>
                        <div 
                            className="col-lg-9 col-md-9">
                            <div className="menu-wrapper">
                                <div id="step-two" className="main-menu">
                                    <nav className="d-none d-lg-block">
                                        <ul id="navigation">
                                            <li><Link to="/">Inicio</Link></li>
                                            <li><Link to="/trabajos">Trabajos</Link></li>
                                            <li><Link to="/acerca">Acerca</Link></li>
                                            <li><Link to="/noticias">Noticias</Link></li>
                                            <li><Link to="/contactenos">Contactenos</Link></li>

                                        </ul>
                                    </nav>
                                </div>
                                <div className="header-btn d-none f-right d-lg-block">
                                    <Link to="/registro" className="btn head-btn1">Registro</Link>
                                    <Link to="/login" className="btn head-btn2">Login</Link>

                                </div>
                            </div>
                        </div>
                       
                        <div className="col-12">
                            <div className="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            </div>
        )}
}

export default Header;
