import React, { Component } from 'react';
class Filtros extends Component {
    state = {}
    render() {
        return (
            
            <div classNameName="job-listing-area pt-120 pb-120">
            <div className="container">
                <div className="row">
                   
                    <div className="col-xl-3 col-lg-3 col-md-4">
                        <div className="row">
                            <div className="col-12">
                                <div className="small-section-tittle2 mb-45">
                                    <div className="ion"> 

                                    {/*<svg xmlns="http://www.w3.org/2000/svg"
                                        xmlns:xlink="http://www.w3.org/1999/xlink" width="20px" height="12px">
                                        <path fill-rule="evenodd" fill="rgb(27, 207, 107)"
                                            d="M7.778,12.000 L12.222,12.000 L12.222,10.000 L7.778,10.000 L7.778,12.000 ZM-0.000,-0.000 L-0.000,2.000 L20.000,2.000 L20.000,-0.000 L-0.000,-0.000 ZM3.333,7.000 L16.667,7.000 L16.667,5.000 L3.333,5.000 L3.333,7.000 Z" />
                                    </svg>
                                    */}

                                    </div>
                                    <h4>Filtrar trabajos</h4>
                                </div>
                            </div>
                        </div>
                        
                        <div className="job-category-listing mb-50">
                            
                            <div className="single-listing">
                                <div className="small-section-tittle2">
                                    <h4>Categorias de trabajo</h4>
                                </div>
                                
                                <div className="select-job-items2">
                                    <select name="select">
                                        <option value="">Todas las categorias</option>
                                        <option value="">Diseñador</option>
                                        <option value="">Marketing</option>
                                        <option value="">Construcción</option>
                                        <option value="">Del hogar</option>
                                    </select>
                                </div>
                                
                                <div className="select-Categories pt-80 pb-50">
                                    <div className="small-section-tittle2">
                                        <h4>Tipo de trabajo</h4>
                                    </div>
                                    <label className="container">Tiempo completo
                                        <input type="checkbox"/>
                                        <span className="checkmark"></span>
                                    </label>
                                    <label className="container">Tiempo parcial
                                        <input type="checkbox" checked=""/>
                                        <span className="checkmark"></span>
                                    </label>
                                    <label className="container">Remoto
                                        <input type="checkbox"/>
                                        <span className="checkmark"></span>
                                    </label>
                                    <label className="container">Freelance
                                        <input type="checkbox"/>
                                        <span className="checkmark"></span>
                                    </label>
                                </div>
                               
                            </div>
                            
                            <div className="single-listing">
                                <div className="small-section-tittle2">
                                    <h4>Localización</h4>
                                </div>
                                
                                <div className="select-job-items2">
                                    <select name="select">
                                        <option value="">En cualquier lugar</option>
                                        <option value="">Guayaquil</option>
                                        <option value="">Cuenca</option>
                                        <option value="">Quito</option>
    
                                    </select>
                                </div>
                                
                                <div className="select-Categories pt-80 pb-50">
                                    <div className="small-section-tittle2">
                                        <h4>Experienca</h4>
                                    </div>
                                    <label className="container">1-2 años
                                        <input type="checkbox"/>
                                        <span className="checkmark"></span>
                                    </label>
                                    <label className="container">2-3 años
                                        <input type="checkbox" checked=""/>
                                        <span className="checkmark"></span>
                                    </label>
                                    <label className="container">3-6 años
                                        <input type="checkbox"/>
                                        <span className="checkmark"></span>
                                    </label>
                                    <label className="container">6-mas
                                        <input type="checkbox"/>
                                        <span className="checkmark"></span>
                                    </label>
                                </div>
                                
                            </div>
                            
                            <div className="single-listing">
                                
                                <div className="select-Categories pb-50">
                                    <div className="small-section-tittle2">
                                        <h4>Publicado desde</h4>
                                    </div>
                                    <label className="container">cualquiera
                                        <input type="checkbox"/>
                                        <span className="checkmark"></span>
                                    </label>
                                    <label className="container">Hoy
                                        <input type="checkbox" checked=""/>
                                        <span className="checkmark"></span>
                                    </label>
                                    <label className="container">Hace 2 días
                                        <input type="checkbox"/>
                                        <span className="checkmark"></span>
                                    </label>
                                    <label className="container">Hace 3 días
                                        <input type="checkbox"/>
                                        <span className="checkmark"></span>
                                    </label>
                                    <label className="container">Hace 5 días
                                        <input type="checkbox"/>
                                        <span className="checkmark"></span>
                                    </label>
                                    <label className="container">Hace 10 días
                                        <input type="checkbox"/>
                                        <span className="checkmark"></span>
                                    </label>
                                </div>
                                
                            </div>
                            <div className="single-listing">
                                
                                <aside className="left_widgets p_filter_widgets price_rangs_aside sidebar_box_shadow">
                                    <div className="small-section-tittle2">
                                        <h4>Filtrar trabajos</h4>
                                    </div>
                                    <div className="widgets_inner">
                                        <div className="range_item">
                                            
                                            <input type="text" className="js-range-slider" value="" />
                                            <div className="d-flex align-items-center">
                                                <div className="price_text">
                                                    <p>Precio :</p>
                                                </div>
                                                <div className="price_value d-flex justify-content-center">
                                                    <input type="text" className="js-input-from" id="amount" readonly />
                                                    <span>to</span>
                                                    <input type="text" className="js-input-to" id="amount2" readonly />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </aside>
                                
                            </div>
                        </div>
                       
                    </div>
                    
                    <div className="col-xl-9 col-lg-9 col-md-8">
                        
                        <div className="featured-job-area">
                            <div id="trabajos" className="container">
                                
                                <div className="row">
                                    <div className="col-lg-12">
                                        <div className="count-job mb-35">
                                            <span>39, 782 Busquedas encontradas</span>
                                            
                                            <div className="select-job-items">
                                                <span>Ordenar por</span>
                                                <select name="select">
                                                    <option value="">Ninguno</option>
                                                    <option value="">Trabajo</option>
                                                    <option value="">Precio</option>
                                                    <option value="">Publicación</option>
                                                </select>
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>
                                
    
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

        );
    }
}

export default Filtros;