import React, { Component } from 'react';
class Experiencia extends Component {
    state = {}
    render() {
        return (
            <li className="timeline-sm-item">
                <span className="timeline-sm-date">{this.props.fecha}</span>
                <h5 className="mt-0 mb-1">{this.props.profesion}</h5>
                <p>{this.props.nominacion}</p>
                <p className="text-muted mt-2">{this.props.descripcion}</p>
            </li>
        );
    }
}

export default Experiencia;

