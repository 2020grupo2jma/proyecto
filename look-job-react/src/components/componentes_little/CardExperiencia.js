import React, { Component } from 'react';
import Experiencia from './Experiencia'
class CardExperiencia extends Component {
    state = {}
    render() {
        return (
            <div className="tab-pane show active" id="about-me">

                <h5 className="mb-4 text-uppercase"><i className="mdi mdi-briefcase mr-1"></i>
                                Experiencia</h5>

                <ul className="list-unstyled timeline-sm">
                    <Experiencia fecha="2015 - 19" profesion="Lead designer / Developer" nominacion="websitename.com" descripcion="Todo el mundo se da cuenta de por qué un nuevo lenguaje
                        común Sería deseable: uno podría negarse a pagar a traductores caros. Para lograr esto, sería necesario tener una gramática uniforme,pronunciación y palabras más comunes."/>
                    <Experiencia fecha="2012 - 15" profesion="Senior Graphic Designer" nominacion="Software Inc." descripcion="Si varios idiomas se fusionan, la gramática del lenguaje resultante es más simple y regular que el de
                        los idiomas individuales. El nuevo lenguaje común será más simple y regular que las lenguas europeas existentes."/>
                    <Experiencia fecha="2010 - 12" profesion="Graphic Designer" nominacion="Coderthemes LLP" descripcion="The European languages are members of
                        the same family. Their separate existence is a myth. For science music sport etc, Europe uses the same vocabulary. The languages only differ in their grammar their pronunciation."/>

                </ul>



            </div>
        );
    }
}

export default CardExperiencia;

