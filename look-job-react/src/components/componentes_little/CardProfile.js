import React, { Component } from 'react';
class CardProfile extends Component {
    state = {}
    render() {
        return (

            <div className="card-box text-center">
                <img src="https://bootdey.com/img/Content/avatar/avatar7.png"
                    className="rounded-circle avatar-xl img-thumbnail" alt="profile-image" />
                <h4 className="mb-0">{this.props.name}</h4>
                <p className="text-muted">{this.props.profesion}</p>
                <button type="button" className="btn btn-danger btn-xs waves-effect mb-2 waves-light">Mensaje</button>
                <div className="text-left mt-3">
                    <h4 className="font-13 text-uppercase">Sobre mi :</h4>
                    <p className="text-muted font-13 mb-3">{this.props.descripcion}
                    </p>
                    <p className="text-muted mb-2 font-13"><strong>Nombre Completo :</strong> <span className="ml-2">{this.props.name}</span></p>

                    <p className="text-muted mb-2 font-13"><strong>Telefono :</strong><span className="ml-2" >{this.props.telefono}</span></p>

                    <p className="text-muted mb-2 font-13"><strong>Email :</strong> <span
                        className="ml-2 ">{this.props.email}</span></p>

                    <p className="text-muted mb-1 font-13"><strong>Ubicación :</strong> <span
                        className="ml-2">Ecuador</span>
                    </p>
                </div>

                <ul className="social-list list-inline mt-3 mb-0">
                    <li className="list-inline-item">
                        <a href="javascript: void(0);" className="social-list-item border-purple text-purple"><i
                            className="fab fa-facebook"></i></a>
                    </li>
                    <li className="list-inline-item">
                        <a href="javascript: void(0);" className="social-list-item border-danger text-danger"><i
                            className="fab fa-google"></i></a>
                    </li>
                    <li className="list-inline-item">
                        <a href="javascript: void(0);" className="social-list-item border-info text-info"><i
                            className="fab fa-twitter"></i></a>
                    </li>
                    <li className="list-inline-item">
                        <a href="javascript: void(0);" className="social-list-item border-secondary text-secondary"><i
                            className="fab fa-github"></i></a>
                    </li>
                </ul>



            </div >

        );
    }
}

export default CardProfile;

