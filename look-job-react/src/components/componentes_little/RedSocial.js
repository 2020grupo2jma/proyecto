import React, { Component } from 'react';
class RedSocial extends Component {
    state = {}
    render() {
        return (
            <div className="col-md-6">
                <div className="form-group">
                    <label for={this.props.lab}>{this.props.name}</label>
                    <div className="input-group">
                        <div className="input-group-prepend">
                            <span className="input-group-text"><i
                                className={this.props.clase}></i></span>
                        </div>
                        <input type="text" className="form-control" id={this.props.lab}
                            placeholder={this.props.placehol} />
                    </div>
                </div>
            </div>
        );
    }
}

export default RedSocial;

