import React, { Component } from 'react';
import RedSocial from './RedSocial';
class CardInformacion extends Component {
    state = {}
    render() {
        return (
            <div className="tab-pane" id="settings">
                <form>
                    <h5 className="mb-3 text-uppercase bg-light p-2"><i className="mdi mdi-account-circle mr-1"></i>
                                    Información Personal</h5>

                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label for="firstname">Primer nombre</label>
                                <input type="text" className="form-control" id="firstname"
                                    placeholder="Ingresa tus nombres" />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label for="lastname">Apellido</label>
                                <input type="text" className="form-control" id="lastname"
                                    placeholder="Ingresa tus Apellidos" />
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-12">
                            <div className="form-group">
                                <label for="userbio">Biografia</label>
                                <textarea className="form-control" id="userbio" rows="4"
                                    placeholder="Escribe sobre ti..."></textarea>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label for="useremail">Correo Electronico</label>
                                <input type="email" className="form-control" id="useremail"
                                    placeholder="Ingresa tu emalil" />
                                <span className="form-text text-muted"><small>Si desea cambiar el correo
                                electrónico
                                                    Por favor <a href="javascript: void(0);">click</a>
                                                    here.</small></span>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label for="userpassword">Contraseña</label>
                                <input type="password" className="form-control" id="userpassword"
                                    placeholder="Ingrese contraseña" />
                                <span className="form-text text-muted"><small>Si quieres cambiar la contraseña
                                                    Por favor <a href="javascript: void(0);">click</a>
                                                    here.</small></span>
                            </div>
                        </div>
                    </div>

                    <h5 className="mb-3 text-uppercase bg-light p-2"><i
                        className="mdi mdi-office-building mr-1"></i> Información de la compañía</h5>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label for="companyname">Nombre de empresa</label>
                                <input type="text" className="form-control" id="companyname"
                                    placeholder="Ingrese el nombre de empresa" />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label for="cwebsite">Sitio Web</label>
                                <input type="text" className="form-control" id="cwebsite"
                                    placeholder="Ingrese sitio web" />
                            </div>
                        </div>
                    </div>

                    <h5 className="mb-3 text-uppercase bg-light p-2"><i className="mdi mdi-earth mr-1"></i>
                                    Social</h5>
                    <div className="row">
                        <RedSocial lab="social-fb" name="Facebook" clase="fab fa-facebook-square" placehol="Url" />
                        <RedSocial lab="social-tw" name="Twitter" clase="fab fa-twitter" placehol="Username" />
                    </div>
                    <div className="row">
                        <RedSocial lab="social-insta" name="Instagram" clase="fab fa-instagram" placehol="Url" />
                        <RedSocial lab="social-lin" name="Linkedin" clase="fab fa-linkedin" placehol="Url" />
                    </div>
                    <div className="row">
                        <RedSocial lab="social-sky" name="Skype" clase="fab fa-skype" placehol="@username" />
                        <RedSocial lab="social-gh" name="Github" clase="fab fa-github" placehol="Username" />
                    </div>
                    <div className="text-right">
                        <button type="submit" className="btn btn-success waves-effect waves-light mt-2"><i
                            className="mdi mdi-content-save"></i> Guardar</button>
                    </div>
                </form>
            </div>
        );
    }
}

export default CardInformacion;

