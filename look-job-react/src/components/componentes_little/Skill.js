import React, { Component } from 'react';
class Skill extends Component {
    state = {}
    render() {
        return (
            <div className="pt-1">
                <h6 className="text-uppercase mt-0">{this.props.habilidad} <span className="float-right">{this.props.porcentaje}%</span></h6>
                <div className="progress progress-sm m-0">
                    <div className="progress-bar bg-purple" role="progressbar" aria-valuenow="90" aria-valuemin="0"
                        aria-valuemax="100" >
                        <span className="sr-only">{this.props.porcentaje} % Complete</span>
                    </div>
                </div>
            </div>
        );
    }
}

export default Skill;

