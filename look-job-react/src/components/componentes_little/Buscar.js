import React, { Component } from 'react';
class Buscar extends Component {
    state = {}
    render() {
        return (
            <div className="input-group md-form form-sm form-2 pl-0">
                <input id="entrada" class="form-control my-0 py-1 lime-border" type="text" placeholder="Search" aria-label="Search"/>
                <div className="input-group-append">
                    <span className="input-group-text lime lighten-2" id="basic-text1"><i className="fas fa-search text-grey"
                    aria-hidden="true"></i></span>
                </div>
            </div>
        );
    }
}

export default Buscar;