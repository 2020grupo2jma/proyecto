import React, { Component } from 'react';
class Card extends Component {
    state = {}
    render() {
        return (
            <div className="col-xl-3 col-md-6 mb-4">
                <div className="card border-0 shadow">
                    <img src={this.props.srcImage} className="card-img-top" alt="..." />
                    <div className="card-body text-center">
                        <h5 className="card-title mb-0">{this.props.name}</h5>
                        <div className="card-text text-black-50">{this.props.profesion}</div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Card;

