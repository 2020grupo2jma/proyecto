import React, { Component } from 'react';
import Skill from './Skill'
class CardSkills extends Component {
    state = {}
    render() {
        return (
            
            <div className="card-box">
                <h4 className="header-title">Skills</h4>
                    <p className="mb-3">Todos se dan cuenta de por qué sería deseable un nuevo lenguaje común</p>
                    <Skill habilidad="HTML5" porcentaje="90" />
                    <Skill habilidad="PHP" porcentaje="67" />
                    <Skill habilidad="WordPress" porcentaje="48" />
                    <Skill habilidad="Laravel" porcentaje="95" />
                    <Skill habilidad="ReactJs" porcentaje="72" />
            </div>
        );
    }
}

export default CardSkills;

