import React, { Component } from 'react';
class HeaderSlide extends Component {
    state = {  }
    render() { 
        return ( 
            <div className="slider-area ">
            <div className="single-slider section-overly slider-height2 d-flex align-items-center"
               style={{backgroundImage: `url(${this.props.urlImage})`}} >
                <div className="container">
                    <div className="row">
                        <div className="col-xl-12">
                            <div className="hero-cap text-center">
                                <h2>{this.props.title}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         );
    }
}
 
export default HeaderSlide;