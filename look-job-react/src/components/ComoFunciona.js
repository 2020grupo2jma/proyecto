import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/Header.css'
import "../assets/css/themify-icons.css"
import "../assets/css/slick.css"
import "../assets/css/nice-select.css"
import "../assets/css/style.css"
import bgCF from '../assets/img/gallery/how-applybg.png'
class ComoFunciona extends Component {
    state = {  }
    render() { 
        return (  <div className="apply-process-area apply-bg pt-150 pb-150" style={{backgroundImage: `url(${bgCF})`}}>
        <div className="container">

            <div class="row">
                <div class="col-lg-12">
                    <div class="section-tittle white-text text-center">
                        <span>Proceso de aplicación</span>
                        <h2> ¿Cómo funciona?</h2>
                    </div>
                </div>
            </div>
           
            <div className="row">
                <div className="col-lg-4 col-md-6">
                    <div className="single-process text-center mb-30">
                        <div class="process-ion">
                            <span class="flaticon-search"></span>
                        </div>
                        <div data-step="6" data-intro="Busca  el trabajo de tu preferencia"
                            data-title="Buscar trabajo" class="process-cap">
                            <h5>1. Busca un trabajo</h5>
                            <p>Navega entre las distintas opciones de trabajo que mas te agraden.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-process text-center mb-30">
                        <div class="process-ion">
                            <span class="flaticon-curriculum-vitae"></span>
                        </div>
                        <div data-step="7" data-intro="Puedes solitar trabajo el que prefieras"
                            data-title="Solicita trabajo" class="process-cap">
                            <h5>2. Solicita trabajo</h5>
                            <p>Ingresa a la opción que mas te guste y mira los requerimientos.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-process text-center mb-30">
                        <div class="process-ion">
                            <span class="flaticon-tour"></span>
                        </div>
                        <div data-step="8" data-intro="Ahora si tienes el trabajo que quieras"
                            data-title="Consigue trabajo" class="process-cap">
                            <h5>3. Consigue tu trabajo</h5>
                            <p>Dale clic en aplicar ahora y ya estarás aplicando para esa oferta de trabajo.</p>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div> );
    }
}
 
export default ComoFunciona;