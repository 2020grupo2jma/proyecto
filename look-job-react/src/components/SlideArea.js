import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/Header.css'
import hero from '../img/hero/h1_hero.jpg'
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/Header.css'
import "../assets/css/themify-icons.css"
import "../assets/css/slick.css"
import "../assets/css/nice-select.css"
import "../assets/css/style.css"

class SlideArea extends React.Component{
    render(){
        return ( 
            <div className="slider-area ">
    
            <div className="slider-active">
                <div class="single-slider slider-height d-flex align-items-center" style={{backgroundImage: `url(${hero})`}}>
                    <div className="container">
                        <div className="row">
                            <div className="col-xl-6 col-lg-9 col-md-10">
                                <div className="hero__caption">
                                    <h1>Encuentra los trabajos mas interesantes</h1>
                                </div>
                            </div>
                        </div>
               
                        <div className="row">
                            <div class="col-xl-8">
                   
                                <form action="#" class="search-box">
                                    <div class="input-form">
                                        <input type="text" placeholder="Puesto, empresa o palabra clave"/>
                                    </div>
                                    <div class="select-form">
                                        <div class="select-itms">
                                            <select name="select" id="select1">
                                                <option value="">Guayaquil</option>
                                                <option value="">Quito</option>
                                                <option value="">Cuenca</option>
                                                <option value="">Machala</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div data-step=" 3" data-intro="Busca el trabajo  por tu zona" data-title="Buscador"
                                        class="search-form">
                                        <a href="#">Buscar</a>

                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

            
        )}
}

export default SlideArea;