import React, { Component} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/Header.css'
import "../assets/css/themify-icons.css"
import "../assets/css/slick.css"
import "../assets/css/nice-select.css"
import "../assets/css/style.css"

import '../assets/css/fontawesome-all.min.css'

class Footer extends Component {
    state = {  }
    render() { 
        return (  
            <div class="footer-bottom-area footer-bg">
                <div class="container">
                    <div class="footer-border">
                        <div class="row d-flex justify-content-between align-items-center">
                            <div class="col-xl-10 col-lg-10 ">
                                <div class="footer-copy-right">
                                    <p>
                                        
                                        Copyright &copy;<script>
                                            document.write(new Date().getFullYear());
                                        </script> Aplicacion realizada por <i class="fa fa-heart"
                                            aria-hidden="true"></i>
                                        <a href="/team_work" target="_blank">Team 2JMA</a>
                                       
                                    </p>

                                </div>

                            </div>
                            <div class="col-xl-2 col-lg-2">
                                <div class="footer-social f-right">

                                    <a href="/"><i class="fab fa-facebook-f"></i></a>
                                    <a href="/"><i class="fab fa-twitter"></i></a>
                                    <a href="/"><i class="fas fa-globe"></i></a>
                                    <a href="/"><i class="fab fa-behance"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
 
export default Footer;