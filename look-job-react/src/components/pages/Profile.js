import React, { Component } from 'react'
import CardProfile from '../componentes_little/CardProfile'
import CardExperiencia from '../componentes_little/CardExperiencia'
import CardSkills from '../componentes_little/CardSkills';
import CardInformacion from '../componentes_little/CardInformacion';


class Profile extends Component {
    state = {}
    render() {
        return (
            <div>
                <link rel="stylesheet"
                    href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.css"
                    integrity="sha256-NAxhqDvtY0l4xn+YVa6WjAcmd94NNfttjNsDmNatFVc=" crossorigin="anonymous" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css"
                    integrity="sha256-2XFplPlrFClt0bIdPgpz8H7ojnk10H69xRqd9+uTShA=" crossorigin="anonymous" />
                <div className="container">
                    <div className="row">
                        <div class="col-lg-4 col-xl-4">

                            <CardProfile name="Mark G. Patel" profesion="@webdesigner" descripcion="Hola, soy Johnathn Deo, ha sido el texto de relleno estándar de la industria desde el
                            1500, cuando un impresor desconocido tomó una galera de tipos." telefono="(123)
                            123 1234" email="user@email.domain"/>
                            <CardSkills />
                            <div className="col-lg-8 col-xl-8">
                                <div className="card-box">
                                    <ul className="nav nav-pills navtab-bg">
                                        <li className="nav-item">
                                            <a href="#about-me" data-toggle="tab" aria-expanded="true" className="nav-link ml-0 active">
                                                <i className="mdi mdi-face-profile mr-1"></i>Acerca de mi
                            </a>
                                        </li>
                                        <li className="nav-item">
                                            <a href="#settings" data-toggle="tab" aria-expanded="false" className="nav-link">
                                                <i className="mdi mdi-settings-outline mr-1"></i>Configuración
                            </a>
                                        </li>
                                    </ul>

                                    <div className="tab-content">
                                        <CardExperiencia />
                                        <CardInformacion />

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default Profile;
