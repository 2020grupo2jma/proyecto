import React, { Component } from 'react'
import HeaderSlide from '../componentes_little/HeaderSlide'
import header from '../../img/header/news.png'

class Conctactenos extends Component {
    state = {  }
    render() { 
        return (<div>
            <HeaderSlide title="Contactenos" urlImage={header}/>
            <section class="section pb-5">

            <div class="row">

                <div class="col-lg-5 mb-4">
                    
                    <div class="card">
                        <div class="card-body">
                    
                            <div class="form-header blue accent-1">
                                <h3><i class="fas fa-envelope"></i>Contáctenos</h3>
                            </div>



                    
                            <div class="md-form">
                                <i class="fas fa-user prefix grey-text"></i>
                                <input type="text" id="form-name" class="form-control"/>
                                <label for="form-name">Nombre</label>
                            </div>
                            <div class="md-form">
                                <i class="fas fa-user prefix grey-text"></i>
                                <input type="text" id="form-name2" class="form-control"/>
                                <label for="form-name">Apellido</label>
                            </div>
                            <div class="md-form">

                                <input type="date" id="form-date" class="form-control"/>
                                <label for="form-email">Fecha de Nacimiento</label>
                            </div>


                            <div class="md-form">
                                <i class="fas fa-envelope prefix grey-text"></i>
                                <input type="text" id="form-email" class="form-control"/>
                                <label for="form-email">Email</label>
                            </div>

                            <div class="md-form">
                                <i class="fas fa-tag prefix grey-text"></i>
                                <input list="places" id="form-Subject" class="form-control"/>
                                <label for="form-Subject">Lugares</label>
                                
                            </div>

                            <div class="md-form">
                                <i class="fas fa-pencil-alt prefix grey-text"></i>
                                <textarea id="form-text" class="form-control md-textarea" rows="3"></textarea>
                                <label for="form-text">Mensaje</label>
                            </div>

                            <div class="text-center mt-4">
                                <button class="btn btn-light-blue">Submit</button>
                            </div>

                        </div>

                    </div>
                    

                </div>
               

                <div class="col-lg-7">

                    
                

               
                  
                    <div class="row text-center">
                        <div class="col-md-4">
                            <a class="btn-floating blue accent-1"><i class="fas fa-map-marker-alt"></i></a>
                            <p>Espol, Guayas</p>
                            <p>Ecuador</p>
                        </div>

                        <div class="col-md-4">
                            <a class="btn-floating blue accent-1"><i class="fas fa-phone"></i></a>
                            <p>+ 01 234 567 89</p>
                            <p>Mon - Fri, 8:00-22:00</p>
                        </div>

                        <div class="col-md-4">
                            <a class="btn-floating blue accent-1"><i class="fas fa-envelope"></i></a>
                            <p>info@gmail.com</p>
                            <p>sale@gmail.com</p>
                        </div>
                    </div>

                </div>
               

            </div>

        </section>    
        </div>  );
    }
}
 
export default Conctactenos;