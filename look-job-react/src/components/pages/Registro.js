import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../css/Header.css'
import "../../assets/css/themify-icons.css"
import "../../assets/css/slick.css"
import "../../assets/css/nice-select.css"
import "../../assets/css/style.css"
import "../../assets/css/slicknav.css"
import "../../assets/css/flaticon.css"

class Registro extends Component {
    state = {  }
    render() { 
        return ( 
        <div className="container h-100">

        <div id="register-form">
            <div className="row">
                <div class="col-md-20 mb-4">
                        <form className="text-center border border-light p-5">

                            <p className="h4 mb-4">Regístrate</p>

                            <div className="form-row mb-4">
                                <div className="col">
                                    
                                    <input type="text" id="defaultRegisterFormFirstName" className="form-control"
                                        placeholder="Nombre"/>
                                </div>
                                <div className="col">
                                  
                                    <input type="text" id="defaultRegisterFormLastName" className="form-control"
                                        placeholder="Apellido"/>
                                </div>
                            </div>

                    
                            <input type="email" id="defaultRegisterFormEmail" className="form-control mb-4"
                                placeholder="E-mail"/>


                            <input type="password" id="defaultRegisterFormPassword" className="form-control"
                                placeholder="Contraseña" aria-describedby="defaultRegisterFormPasswordHelpBlock"/>
                            <small id="defaultRegisterFormPasswordHelpBlock" className="form-text text-muted mb-4">
                                Al menos 8 caracteres y 1 dígito
                            </small>

                       
                            <input type="text" id="defaultRegisterPhonePassword" className="form-control"
                                placeholder="Phone number" aria-describedby="defaultRegisterFormPhoneHelpBlock"/>
                            <small id="defaultRegisterFormPhoneHelpBlock" className="form-text text-muted mb-4">
                                Opcional: para autenticación en dos pasos
                            </small>

                          
                            <div className="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input"
                                    id="defaultRegisterFormNewsletter"/>
                                <label className="custom-control-label" for="defaultRegisterFormNewsletter">Suscríbete a nuestro boletín</label>
                            </div>

                
                            <button className="btn btn-info my-4 btn-block waves-effect waves-light" type="submit">Registrarse</button>




                        </form>
                    





                </div>
               

            </div>
     

        </div>


    </div> );
    }
}
 
export default Registro;