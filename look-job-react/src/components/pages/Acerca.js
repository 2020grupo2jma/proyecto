import React, { Component } from 'react'
import HeaderSlide from '../componentes_little/HeaderSlide'
import header from '../../img/header/news.png'
import imgAcerca from  '../../img/service/support-img.jpg'

class Acerca extends Component {
    state = {  }
    render() { 
        return ( 
<div>
<HeaderSlide title="Acerca" urlImage={header}/>
<div className="support-company-area fix section-padding2">
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-xl-6 col-lg-6">
                        <div className="right-caption">
                            
                            <div className="section-tittle section-tittle2">
                                <span>¿Que somos?</span>
                                <h2>Plataforma de busqueda de empleo</h2>
                            </div>
                            <div className="support-caption">
                                <p className="pera-top">La idea central es otorgar un sistema que aporte ayuda a los
                                    ciudadanos de la ciudad de Guayaquil,debido a la situacion actualque se esta
                                    viviendo un gran porcentaje de personas
                                    perdieron sus plazas de trabajos. Por tal motivo los integrantes del grupo 2JMA
                                    realizaran una
                                    aplicacion web que permita a los usuarios ofrecerles ocupaciones de trabajo que
                                    ellos requieran en
                                    tiempo real.Es decir que en esta aplicacion si un usuario requiere de servicios de
                                    carpinteria, mecanica,
                                    entre otros el usuario podrá contactar a dicha persona para que de esta manera la
                                    persona que
                                    ofrezca sus servicios se acerque al cliente mediante su ubicación..</p>
                                <p> aplicación presentara una evaluación de cada servicio que ofrece cada persona para
                                    que de esta manera el cliente verifique que sea confiable, asi el cliente tendrá la
                                    aopcion de acercarse a las instalaciones del emprendedor para acceder a sus
                                    servicios o el trabajador irá a realizar el trabajo a las residencias gracias al
                                    acceso de las
                                    ubicaciones de los clientes. Por medio del menú de la aplicación podrá encontrar una
                                    variedad de información que
                                    será de gran utilidad para el cliente, tambien contará con la versatilidad de
                                    ofrecer servicios por medio
                                    de videollamdas en casos que no requieran interacción personal.</p>
                                <a href="jobs_postule.html" className="btn post-btn">Postula a un trabajo</a>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-6 col-lg-6">
                        <div className="support-location-img">
                            <img src={imgAcerca} alt=""/>
                            <div className="support-img-cap text-center">
                                <p>Desde</p>
                                <span>2020</span>
                            </div>
                    </div>
                       
                    </div>
                </div>
            </div>
        </div>
</div>
          
         );}
}
 
export default Acerca;