import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../css/Header.css'
import "../../assets/css/themify-icons.css"
import "../../assets/css/slick.css"
import "../../assets/css/nice-select.css"
import "../../assets/css/style.css"
import "../../assets/css/slicknav.css"
import "../../assets/css/flaticon.css"
import logo from '../../img/logo/logo.png';
class Login extends Component {
    state = {  }
    render() { 
        return (  
        <div className="container">
        <div className="row h-100 justify-content-center align-items-center">
            <div className="row">
                <div className="col-sm-20 col-md-20 col-md-offset-4">
                    <h1 className="text-center login-title">Inicia sesión para continuar BUSCANDO EMPLEOS</h1>
                    <div className="account-wall">
                        <img className="profile-img" src={logo} alt=""/>
                        <form className="form-signin">
                            <input type="text" className="form-control" placeholder="Email" required autofocus/>
                            <input type="password" className="form-control" placeholder="Password" required/>
                            <button className="btn btn-lg btn-primary btn-block" type="submit">
                                Iniciar sesión</button>
                            <label className="checkbox pull-left">
                                <input type="checkbox" value="remember-me"/>
                                Recuerdame
                            </label>
                            
                        </form>
                    </div>
                    <Link to="/registro" className="text-center new-account">Crear una cuenta </Link>
                </div>
            </div>
        </div>
    </div>);
    }
}
 
export default Login;