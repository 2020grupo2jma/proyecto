import React, { Component } from 'react'
import Card from '../componentes_little/Card'
import e1 from '../../img/team/1.jpeg'
import e2 from '../../img/team/2.jpg'
import e3 from '../../img/team/3.jpeg'
import e4 from '../../img/team/4.jpeg'

class Team_work extends Component {
    state = {}
    render() {
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <Card name="Joseph Avila" srcImage={e1} profesion=" Student" />
                        <Card name="Andres Morales" srcImage={e2} profesion="Student" />
                        <Card name="Juliana Riera" srcImage={e3} profesion="Student" />
                        <Card name="Moisés Atupaña" srcImage={e4} profesion="Student" />


                    </div>


                </div>
            </div>

        );
    }
}

export default Team_work;