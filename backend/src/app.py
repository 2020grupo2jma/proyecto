from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:rootroot@localhost/lookingjob'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
ma = Marshmallow(app)
#Database
class User(db.Model):
    id=db.Column(db.Integer, primary_key=True)
    name=db.Column(db.String(50))
    lastname=db.Column(db.String(50))
    email=db.Column(db.String(30))
    password=db.Column(db.String(50))
    phone=db.relationship('Phone',backref='phone')
    address=db.relationship('Address',backref='address')
    def __init__(self, name,lastname,email,password):
        self.name=name
        self.lastname=lastname
        self.email=email
        self.password=password
       
class Phone(db.Model):
    
    id = db.Column(db.Integer, primary_key=True)
    movil = db.Column(db.String(10), unique=True)
    convencional = db.Column(db.String(10))
    person_id=db.Column(db.Integer,db.ForeignKey('user.id'))
    

    
class Address(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    streetprincipal = db.Column(db.String(50))
    streetsecondary = db.Column(db.String(50))
    city=db.Column(db.String(50))
    person_id=db.Column(db.Integer,db.ForeignKey('user.id'))
    

 


db.create_all()

class UserSchema(ma.Schema):
    class Meta:
        fields = ('id','name','lastname','email','password','phone','address')
user_schema = UserSchema()
users_schema =UserSchema(many=True)
#Routes
@app.route('/users',methods=['POST'])
def create_user():
    name=request.json['name']
    lastname=request.json['lastname']
    email=request.json['email']
    password=request.json['password']
    phone=Phone(request.json['phone'],request.json['phone'])
    address=Address(request.json['address'],request.json['address'],request.json['address'])
    new_user=User(name,lastname,email,password)
    new_phone=Phone(phone=email)
    new_address=Address(address=email)
    db.session.add(new_user)
    db.session.add(new_phone)
    db.session.add(new_address)
    data=db.session.commit()
    print(data)
    
    return users_schema.jsonify(new_user)
if __name__ == '__main__':
    app.run(debug=True)






