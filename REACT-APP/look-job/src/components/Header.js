import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/Header.css'
import logo from '../img/logo/logo.png';
class Header extends React.Component{

    render(){
        return (
            <div>
            <div className="header-area header-transparrent">
            <div className="headder-top header-sticky">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-lg-3 col-md-2">                          
                      <a href="index.html"><img src={logo} /></a>  
                        </div>
                        <div 
                            className="col-lg-9 col-md-9">
                            <div className="menu-wrapper">
                                <div id="step-two" className="main-menu">
                                    <nav className="d-none d-lg-block">
                                        <ul id="navigation">
                                            <li><a href="index.html">Inicio</a></li>
                                            <li><a href="about.html">Acerca</a></li>
                                            <li><a href="news.html">Noticias</a></li>
                                            <li><a href="contactenos.html">Contactenos</a></li>

                                        </ul>
                                    </nav>
                                </div>
                                <div className="header-btn d-none f-right d-lg-block">
                                    <a href="register.html" className="btn head-btn1">Registro</a>
                                    <a href="login.html" className="btn head-btn2">Login</a>

                                </div>
                            </div>
                        </div>
                       
                        <div className="col-12">
                            <div className="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            </div>
        )}
}

export default Header;
