import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/Header.css'

class SlideArea extends React.Component{
    render(){
        return ( 
            <div class="slider-area ">

            <div class="slider-active">
                <div class="single-slider slider-height d-flex align-items-center"
                    data-background="assets/img/hero/h1_hero.jpg">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-6 col-lg-9 col-md-10">
                                <div class="hero__caption">
                                    <h1>Encuentra los trabajos mas interesantes</h1>
                                </div>
                            </div>
                        </div>
          
                        <div class="row">
                            <div class="col-xl-8">
                     
                                <form action="#" class="search-box">
                                    <div class="input-form">
                                        <input type="text" placeholder="Puesto, empresa o palabra clave"/>
                                    </div>
                                    <div class="select-form">
                                        <div class="select-itms">
                                            <select name="select" id="select1">
                                                <option value="">Guayaquil</option>
                                                <option value="">Quito</option>
                                                <option value="">Cuenca</option>
                                                <option value="">Machala</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div data-step=" 3" data-intro="Busca el trabajo  por tu zona" data-title="Buscador"
                                        class="search-form">
                                        <a href="#">Buscar</a>

                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

            
        )}
}

export default SlideArea;