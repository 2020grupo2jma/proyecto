import React from 'react';
import '../style/LeftAdmin.css'
import 'bootstrap/dist/css/bootstrap.min.css';
class LeftAdmin extends React.Component {
    render(){
        return(
            <div>
             <div class="skin-blue">
    <div class="wrapper">
        <header class="main-header">
         
            <nav class="navbar navbar-static-top" role="navigation">
        
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-custom-menu"></div>
            </nav>
        </header>
        <aside class="main-sidebar">
            <section class="sidebar">
                <ul class="sidebar-menu">
                    <li class="header">ADMINISTRADORES</li>
                    <li role="presentation" class="active">
                        <a href="dashboard.html"><i class="fas fa-home"></i>&nbsp; INICIO</a>
                    </li>

                    <li role="presentation" class="">
                        <a href="disponibilidad.html"><i class="fas fa-user-check"></i>&nbsp; NOTICIAS</a>
                    </li>
                    <li role="presentation" class="">
                        <a href="notificar.html"><i class="fas fa-bullhorn"></i>&nbsp;EMPRESAS</a>
                    </li>
                    <li role="presentation" class="">
                        <a href="resultados.html"><i class="fas fa-poll"></i>&nbsp;EMPLEADOS</a>
                    </li>
                    <li role="presentation" class="">
                        <a href="liberarEspacio.html"><i class="ion-speedometer"></i>&nbsp;MENSAJES</a>
                    </li>
                    <li role="presentation" class="">
                        <a href="formulario.html">&nbsp; EMPLEOS</a>
                    </li>
                </ul>
            </section>
            <div class="logout text-white btn ml-3">
                <i class="fas fa-sign-out-alt"></i>
                SALIR
            </div>
        </aside>       
    </div>

   
</div>
</div>




        ) }


}
export default LeftAdmin